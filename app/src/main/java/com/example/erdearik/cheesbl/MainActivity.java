package com.example.erdearik.cheesbl;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.StrictMode;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.Socket;
import java.net.URL;
import java.net.URLConnection;
import java.util.StringTokenizer;
import java.util.Timer;
import java.util.TimerTask;
import java.util.logging.Handler;
import java.util.logging.LogRecord;

import butterknife.Bind;
import butterknife.ButterKnife;

public class MainActivity extends AppCompatActivity {
    private ImageView[] cheesView = new ImageView[65];
    private int[] cheesObject = {
            R.drawable.w_king,
            R.drawable.w_queen,
            R.drawable.w_bishop,
            R.drawable.w_knight,
            R.drawable.w_rook,

            R.drawable.b_king,
            R.drawable.b_queen,
            R.drawable.b_bishop,
            R.drawable.b_knight,
            R.drawable.b_rook,
    };
    private int obj;
    private int col;
    private int row;
    private int i;
    private String data = "k,a,3 q,c,4 b,d,1";
    private String[] dataArr ;//= data.split("<br/>");
    private TextView tv;
    private String stringUrl = "http://mobile.suitmedia.com/bl/chess.php";
    private static final String SERVER  = "xinuc.org";
    private static final Integer PORT   = 7387;
    private Socket socket;
    private ByteArrayOutputStream outputStream;
    private InputStream inputStream;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        if (android.os.Build.VERSION.SDK_INT > 9) {
            StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
            StrictMode.setThreadPolicy(policy);
        }
        tv = (TextView) findViewById(R.id.tv);

        cheesInit();
        Thread t = new Thread(){
            @Override
            public void run() {
                super.run();
                while (!isInterrupted()){
                    try {
                        Thread.sleep(1000);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            for(int inc = 1; inc<65; inc++) cheesView[inc].setVisibility(View.GONE);
                            new AmbilData().execute(stringUrl);
                        }
                    });
                }
            }
        };
        t.start();
    }

    public void createObj(String data){
        this.data = data;
        dataArr = data.split("<br/>");
//        tv.setText(dataArr[9]+" "+dataArr.length);

        for (int inc = 0; inc < dataArr.length-2; inc++)
            parsData(dataArr[inc]);
    }

    protected void parsData(String a){
        String[] hasil = a.split(",");
        setobjek(hasil[0].charAt(0), hasil[1].charAt(0), Integer.parseInt(hasil[2]));
    }
    protected void setobjek(char a, char b, int c){
        setObj(a);
        setCol(b);
        setRow(c);
        cheesView[(row-1)*8+col].setVisibility(0);
        cheesView[(row-1)*8+col].setImageResource(cheesObject[obj]);
    }

    private void setObj(char a){
        switch (a){
            case 'K': obj = 0; break;
            case 'Q': obj = 1; break;
            case 'B': obj = 2; break;
            case 'N': obj = 3; break;
            case 'R': obj = 4; break;
            case 'k': obj = 5; break;
            case 'q': obj = 6; break;
            case 'b': obj = 7; break;
            case 'n': obj = 8; break;
            case 'r': obj = 9; break;
        }
    }

    private void setCol(char b){
        switch (b){
            case 'a': col = 1; break;
            case 'b': col = 2; break;
            case 'c': col = 3; break;
            case 'd': col = 4; break;
            case 'e': col = 5; break;
            case 'f': col = 6; break;
            case 'g': col = 7; break;
            case 'h': col = 8; break;
        }
    }

    private void setRow(int c){
        row = c;
    }

    private void cheesInit(){
        cheesView[1] = (ImageView) findViewById(R.id.a1);
        cheesView[2] = (ImageView) findViewById(R.id.a2);
        cheesView[3] = (ImageView) findViewById(R.id.a3);
        cheesView[4] = (ImageView) findViewById(R.id.a4);
        cheesView[5] = (ImageView) findViewById(R.id.a5);
        cheesView[6] = (ImageView) findViewById(R.id.a6);
        cheesView[7] = (ImageView) findViewById(R.id.a7);
        cheesView[8] = (ImageView) findViewById(R.id.a8);


        cheesView[9] = (ImageView) findViewById(R.id.b1);
        cheesView[10] = (ImageView) findViewById(R.id.b2);
        cheesView[11] = (ImageView) findViewById(R.id.b3);
        cheesView[12] = (ImageView) findViewById(R.id.b4);
        cheesView[13] = (ImageView) findViewById(R.id.b5);
        cheesView[14] = (ImageView) findViewById(R.id.b6);
        cheesView[15] = (ImageView) findViewById(R.id.b7);
        cheesView[16] = (ImageView) findViewById(R.id.b8);


        cheesView[17] = (ImageView) findViewById(R.id.c1);
        cheesView[18] = (ImageView) findViewById(R.id.c2);
        cheesView[19] = (ImageView) findViewById(R.id.c3);
        cheesView[20] = (ImageView) findViewById(R.id.c4);
        cheesView[21] = (ImageView) findViewById(R.id.c5);
        cheesView[22] = (ImageView) findViewById(R.id.c6);
        cheesView[23] = (ImageView) findViewById(R.id.c7);
        cheesView[24] = (ImageView) findViewById(R.id.c8);

        cheesView[25] = (ImageView) findViewById(R.id.d1);
        cheesView[26] = (ImageView) findViewById(R.id.d2);
        cheesView[27] = (ImageView) findViewById(R.id.d3);
        cheesView[28] = (ImageView) findViewById(R.id.d4);
        cheesView[29] = (ImageView) findViewById(R.id.d5);
        cheesView[30] = (ImageView) findViewById(R.id.d6);
        cheesView[31] = (ImageView) findViewById(R.id.d7);
        cheesView[32] = (ImageView) findViewById(R.id.d8);

        cheesView[33] = (ImageView) findViewById(R.id.e1);
        cheesView[34] = (ImageView) findViewById(R.id.e2);
        cheesView[35] = (ImageView) findViewById(R.id.e3);
        cheesView[36] = (ImageView) findViewById(R.id.e4);
        cheesView[37] = (ImageView) findViewById(R.id.e5);
        cheesView[38] = (ImageView) findViewById(R.id.e6);
        cheesView[39] = (ImageView) findViewById(R.id.e7);
        cheesView[40] = (ImageView) findViewById(R.id.e8);

        cheesView[41] = (ImageView) findViewById(R.id.f1);
        cheesView[42] = (ImageView) findViewById(R.id.f2);
        cheesView[43] = (ImageView) findViewById(R.id.f3);
        cheesView[44] = (ImageView) findViewById(R.id.f4);
        cheesView[45] = (ImageView) findViewById(R.id.f5);
        cheesView[46] = (ImageView) findViewById(R.id.f6);
        cheesView[47] = (ImageView) findViewById(R.id.f7);
        cheesView[48] = (ImageView) findViewById(R.id.f8);

        cheesView[49] = (ImageView) findViewById(R.id.g1);
        cheesView[50] = (ImageView) findViewById(R.id.g2);
        cheesView[51] = (ImageView) findViewById(R.id.g3);
        cheesView[52] = (ImageView) findViewById(R.id.g4);
        cheesView[53] = (ImageView) findViewById(R.id.g5);
        cheesView[54] = (ImageView) findViewById(R.id.g6);
        cheesView[55] = (ImageView) findViewById(R.id.g7);
        cheesView[56] = (ImageView) findViewById(R.id.g8);

        cheesView[57] = (ImageView) findViewById(R.id.h1);
        cheesView[58] = (ImageView) findViewById(R.id.h2);
        cheesView[59] = (ImageView) findViewById(R.id.h3);
        cheesView[60] = (ImageView) findViewById(R.id.h4);
        cheesView[61] = (ImageView) findViewById(R.id.h5);
        cheesView[62] = (ImageView) findViewById(R.id.h6);
        cheesView[63] = (ImageView) findViewById(R.id.h7);
        cheesView[64] = (ImageView) findViewById(R.id.h8);
    }

    private class AmbilData extends AsyncTask<String, Void, String> {
        @Override
        protected String doInBackground(String... urls) {

            // params comes from the execute() call: params[0] is the url.
            try {
                return downloadUrl(urls[0]);
            } catch (IOException e) {
                e.printStackTrace();
                return "Unable to retrieve web page. URL may be invalid.";
            }
        }
        // onPostExecute displays the results of the AsyncTask.
        @Override
        protected void onPostExecute(String result) {
            data = result;
            createObj();
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }
    }

    private void createObj(){
        dataArr = data.split("<br/>");
        String has="";

        for (int inc = 0; inc < dataArr.length-2; inc++){
            parsData(dataArr[inc]);
            has += " - "+dataArr[inc];
        }
        tv.setText(has);
    }

    private String downloadUrl(String myurl) throws IOException {
        InputStream is = null;
        // Only display the first 500 characters of the retrieved
        // web page content.
        int len = 500;

        try {
            URL url = new URL(myurl);
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.connect();
            int response = conn.getResponseCode();
            Log.d("Debud Mode Connection", "The response is: " + response);
            is = conn.getInputStream();

            // Convert the InputStream into a string
            String contentAsString= readIt(is, len);
            return contentAsString;
        } finally {
            if (is != null) {
                is.close();
            }
        }
    }

    // Reads an InputStream and converts it to a String.
    public String readIt(InputStream stream, int len) throws IOException, UnsupportedEncodingException {
        Reader reader = null;
        reader = new InputStreamReader(stream, "UTF-8");
        char[] buffer = new char[len];
        reader.read(buffer);
        return new String(buffer);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }
}
